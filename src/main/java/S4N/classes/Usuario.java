package S4N.classes;


import java.util.List;

public class Usuario {

    String nombreUsuario;
    int edad;
    String ciudad;
    List<CuentaBancaria> listaCuentaBancaria;

    public Usuario(String nombreUsuario, int edad, String ciudad) {
        this.nombreUsuario = nombreUsuario;
        this.edad = edad;
        this.ciudad = ciudad;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public List<CuentaBancaria> getListaCuentaBancaria() {
        return listaCuentaBancaria;
    }

    public void setListaCuentaBancaria(List<CuentaBancaria> listaCuentaBancaria) {
        this.listaCuentaBancaria = listaCuentaBancaria;
    }
}
