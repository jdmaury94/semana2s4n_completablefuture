package S4N.classes;

import java.util.concurrent.CompletableFuture;

public class Operaciones {

    public CompletableFuture<Integer> consultarSaldo(CuentaBancaria cuentaBancaria){
        CompletableFuture<Integer> saldoCuenta=CompletableFuture.supplyAsync(cuentaBancaria::getSaldo);
        return saldoCuenta;
    }

    public CompletableFuture<Integer> consultarSaldoDemorado(CuentaBancaria cb){
        CompletableFuture<Integer> saldoCuentaDemorado=CompletableFuture.supplyAsync(()->{
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return cb.getSaldo();
        });
        return saldoCuentaDemorado;
    }

    public CompletableFuture<Void> realizarTransferencia(int monto,CuentaBancaria cb1,CuentaBancaria cb2){
        CompletableFuture<Void> transferencia=CompletableFuture.runAsync(()->{
            cb2.setSaldo(cb2.getSaldo()+monto);
            cb1.setSaldo(cb1.getSaldo()-monto);
        });//.thenRun(()-> System.out.println("Transferencia exitosa, nuevo saldo de cuenta destino: "+saldoCuenta2));
        return transferencia;
    }

    public CompletableFuture<Void> retirarDinero(int monto,CuentaBancaria cb1){
        CompletableFuture<Void> retiro=CompletableFuture.runAsync(()->{
            cb1.saldo=cb1.getSaldo()-monto;
        });//.thenRun(()-> System.out.println("Retiro exitoso. Nuevo saldo es "+cb1.saldo));;
        return retiro;    }

    public CompletableFuture<String> consultarEstadoCuenta(CuentaBancaria cb){
        CompletableFuture<String> estadoCuenta=CompletableFuture.supplyAsync(()->{;
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Consulta demasiado demorada..su estado es: "+cb.getEstadoCuenta();
        });
        return estadoCuenta;
    }

    public CompletableFuture<String> consultarEstadoCuentaDemorado(CuentaBancaria cb){
        CompletableFuture<String> estadoCuenta=CompletableFuture.supplyAsync(()->{
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Consulta demorada..su estado es: "+cb.getEstadoCuenta();
        });
        return estadoCuenta;
    }

    public CompletableFuture<String> consultaTiempoEspera(){
        CompletableFuture<String> valor2segundos=CompletableFuture.supplyAsync(()->{
            try {
                //System.out.print(Thread.currentThread().getName());
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Consulta tardó demasiado, intente nuevamente..";
        });
        return valor2segundos;
    }


    public String differentThreadPool(){
        try {
            Thread.sleep(1000);
            System.out.print(Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "-> Hilo diferente en el mismo pool";
    }
}

