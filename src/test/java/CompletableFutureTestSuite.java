import S4N.classes.CuentaBancaria;
import S4N.classes.Operaciones;
import S4N.classes.Usuario;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

import static org.junit.Assert.*;

public class CompletableFutureTestSuite {

    Operaciones op;
    Usuario usr1,usr2,usr3;
    CuentaBancaria cb1,cb2,cb3,cb4,cb5,cb6,cb7,cb8;

    @Before
    public void initialize(){
        op=new Operaciones();

        usr1=new Usuario("José Daniel",24,"Barranquilla");
        usr2=new Usuario("Andrés",28,"Barranquilla");
        usr3=new Usuario("Marcela",37,"Bogotá");

        cb1=new CuentaBancaria("23490832",430000,"Activa",1982);
        cb2=new CuentaBancaria("32490823",4780000,"Activa",1282);
        cb3=new CuentaBancaria("09239849",1240000,"Inactiva",1452);
        cb4=new CuentaBancaria("12384323",200000,"Inactiva",3902);
        cb5=new CuentaBancaria("21090323",1685000,"Activa",1236);
        cb6=new CuentaBancaria("54532298",23450000,"Activa",6023);
        cb7=new CuentaBancaria("34390323",125000,"Activa",4656);
        cb8=new CuentaBancaria("02933s89",3500,"Activa",4233);

        usr1.setListaCuentaBancaria(Arrays.asList(cb1,cb2,cb3));
        usr2.setListaCuentaBancaria(Arrays.asList(cb4,cb5));
        usr3.setListaCuentaBancaria(Arrays.asList(cb6,cb7,cb8));
    }

    @Test
    public void acceptEitherPrueba(){
        //Consultar saldo cuenta
        //Toma el valor del primer future finalizado
        CompletableFuture<Integer> consultarSaldo=op.consultarSaldo(cb1);
        CompletableFuture<Integer> consultarSaldoDemorado=op.consultarSaldoDemorado(cb1);
        consultarSaldo.acceptEitherAsync(consultarSaldoDemorado,saldo->
                System.out.println("Saldo es de..."+saldo+"\n"));
        int saldo=consultarSaldo.join();
        assertEquals(430000,saldo);
    }

    @Test //Sumar la cantidad total de dinero de usr1
    public void allOfPrueba(){
        CompletableFuture<Integer> futureSaldo1=op.consultarSaldo(cb1);
        CompletableFuture<Integer> futureSaldo2=op.consultarSaldo(cb2);
        CompletableFuture<Integer> futureSaldo3=op.consultarSaldo(cb3);

        CompletableFuture<Void> saldosUsr1=CompletableFuture.allOf(futureSaldo1,futureSaldo2,futureSaldo3);

        List<CompletableFuture<Integer>> lista=Arrays.asList(futureSaldo1,futureSaldo2,futureSaldo3);
        CompletableFuture<Integer> sumaSaldosFuture=saldosUsr1.thenApply(future->{
            return lista.stream().mapToInt(p->p.join()).sum();
        });
        int suma=sumaSaldosFuture.join();
        assertEquals(6450000,suma);
    }

    @Test //Aplicar una función al resultado de consultarEstadoCuenta
    public void applyToEitherConsultaEstadoCuenta(){
        CompletableFuture<String> estadoCuentaFuture=op.consultarEstadoCuenta(cb2);
        CompletableFuture<String> estadoCuentaFutureDemorado=op.consultarEstadoCuentaDemorado(cb2);
        CompletableFuture<String> resultado=estadoCuentaFuture.applyToEitherAsync(estadoCuentaFutureDemorado,s->s);
        System.out.println(resultado.join());
        assertEquals("Consulta demorada..su estado es: "+cb2.getEstadoCuenta(),resultado.join());
    }

    @Test
    public void handlePrueba(){
        String numeroCuenta=cb8.getNumeroCuenta();
        CompletableFuture<String> numeroCuentaToDouble=CompletableFuture.supplyAsync(()->{
            int doubleNumCuenta=Integer.parseInt(numeroCuenta);
            return doubleNumCuenta;
        }).handleAsync((resultado,ex)-> (ex!=null)?"Ha ocurrido un error":" ");
        System.out.println(numeroCuentaToDouble.join());
        assertEquals("Ha ocurrido un error",numeroCuentaToDouble.join());
    }

    @Test //Retirar Dinero y Consultar Saldo
    public void runAfterBothPrueba(){
        CompletableFuture<Void> retiroDineroFuture=op.retirarDinero(150000,cb2);
        CompletableFuture<Integer> consultaSaldoFuture=op.consultarSaldo(cb2);
        consultaSaldoFuture.runAfterBoth(retiroDineroFuture,()-> System.out.println("Se retiró dinero y se actualizó la cuenta"));
    }

    @Test
    public void thenAcceptPrueba(){
       CompletableFuture<Void> transferencia=op.realizarTransferencia(10000,cb2,cb4);
       transferencia.thenRun(()-> System.out.println("Transferencia realizada."));
       CompletableFuture<Integer> consultarSaldo=op.consultarSaldo(cb4);
       int saldo=consultarSaldo.join();
       assertEquals(210000,saldo);
    }

    @Test
    public void thenCombine(){
       CompletableFuture<String> estadoCuenta2=op.consultarEstadoCuenta(cb2);
       CompletableFuture<String> estadoCuenta8=op.consultarEstadoCuenta(cb8);
       CompletableFuture<String> combine=estadoCuenta2.thenCombine(estadoCuenta8,(e1,e2)->e1);
       assertEquals("Consulta demasiado demorada..su estado es: Activa",combine.join());
    }

    @Test
    public void thenCompose(){
        CompletableFuture<Integer> saldo7=op.consultarSaldo(cb7);
        CompletableFuture<Integer> suma=saldo7.thenCompose(s->CompletableFuture.supplyAsync(()->s+250000));
        int result=suma.join();

        assertEquals(375000,result);
    }


    @Test
    public void actualizarCuenta(){
        CompletableFuture<String> quitarCeros=CompletableFuture.supplyAsync(()->cb5.getNumeroCuenta())
                .thenApplyAsync(num->num.replace("0",""));
        String result=quitarCeros.join();
        assertEquals("219323",result);
    }

    @Test
    public void actualizarClave(){
        CompletableFuture<Integer> claveActual=CompletableFuture.supplyAsync(()->cb3.getClave());
        CompletableFuture<Integer> claveActualizada=claveActual.thenApply(clave->9834);

        claveActual.thenApply(clave->1234).thenRun(()-> System.out.println("Clave Actualizada"));
        int newPass=claveActualizada.join();
        assertEquals(9834,newPass);
    }

    @Test
    public void sumaSaldosFromList(){
        CompletableFuture<List<CuentaBancaria>> listaCuentas=CompletableFuture.supplyAsync(()->usr3.getListaCuentaBancaria());
        CompletableFuture<Integer> sumaSaldosLista=listaCuentas.thenApply(listaCuenta->{
            return listaCuenta.stream()
                    .mapToInt(cuenta->cuenta.getSaldo()).sum();
            });
        int suma=sumaSaldosLista.join();
        assertEquals(23578500,suma);
    }

    @Test
    public void runDifferentThreads(){

        ExecutorService customPool = Executors.newFixedThreadPool(3);

        CompletableFuture<Integer> saldoFuture=op.consultarSaldo(cb3);
        CompletableFuture<String> saldoMainThread=saldoFuture.thenApply(s-> Thread.currentThread().getName());
        CompletableFuture<String> saldoMainThreadFork=saldoFuture.thenApplyAsync(s-> Thread.currentThread().getName());
        CompletableFuture<String> saldoMainThreadExecutor=saldoFuture.thenApplyAsync(s-> Thread.currentThread().getName(),customPool);

        assertNotEquals(saldoMainThread.join(),saldoMainThreadFork.join());
        assertNotEquals(saldoMainThreadFork.join(),saldoMainThreadExecutor.join());
    }

    @Test
    public void defaultValueTest()  {
        String result="";
        CompletableFuture<String> stringDemorado=op.consultaTiempoEspera();
        try {
            result=stringDemorado.get(1, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            result="Consulta tardó demasiado, intente nuevamente..";
        }
        assertEquals("Consulta tardó demasiado, intente nuevamente..",result);
    }

    @Test
    public void submitAsync() throws ExecutionException, InterruptedException {
        CompletableFuture<String> resultado;
        ExecutorService customPool = Executors.newFixedThreadPool(4);
        for(int i=1;i<=4;i++){
            resultado=op.consultaTiempoEspera();
            resultado.thenRun(()-> System.out.println(Thread.currentThread().getName()+"-> Hilo actual"));
        }

        for(int i=1;i<=4;i++){
            Future<String> hiloEjecutor=customPool.submit(()->op.differentThreadPool());
            System.out.println(hiloEjecutor.get());
        }
    }


}
